## General TODOs

- [ ] What name? FOSSMO? FOSS{,-}Mobile ?
- [ ] Called a "committee", but what would be more appropriate? Collective?
- [ ] Other TODOs are down below in specific sections

## Responsibilities / Purpose:

### Place for people/projects to submit requests for feedback from community, and way for community to give feedback.
- Committee members can give feedback if interested, but no obligation to give
  feedback on every thing submitted.
- Feedback to those who request it is not a demand or order, they can ignore it.

### List and vote on community priorities. Periodically maintain this list.

- Highlight areas where contributions are most needed
- Publicly recognize work on these
  - (TODO) Or some other meaningful way to reward folks. Non-monetary for now...
- Non-binding, so no obligation for contributors to work on these, and no
  punishment for contributors who choose not to.

### External / Public representation

- Organize presence at events (e.g. FOSDEM)

- Create / maintain public website introducing FOSS on Mobile to non-technical users
  - Can also list community priorities, and have instructions for projects, etc
  to poll community for feedback
  - (TODO) Or something like that, open to other ideas, but want to keep it
  simple for now.

### Other responsibilities

- (TODO) Want to keep the list of responsibilities fairly light for now until
kinks are worked out, but anything major missing?

## Committee composition

- Projects, organizations, companies get 1 representative. They also handle the
selection process, "term limits", and so on.

  - One person can only represent one project, organization, or company.

    - (TODO) The intention is to prevent a single group from being
    over-represented... so maybe there should be something to prevent multiple
    people from a group from being representatives from multiple projects? We
    all wear different "hats", so I'm not sure how to deal with that yet.

  - (TODO) What about accepting new members?

  - (TODO) Where is the line drawn with regards to who is "allowed" to
  participate? What about Android "distros"?

- Changes to committee responsibilities should be approved by 75% of members
  - (TODO) Is 75% OK?

- Ranked Choice Voting preferred in cases where there are >2 choices.

## Committee Operations

- Prefer to operate asynchronously as much as possible, since organizing
meetings will be tough due to timezones, etc.

- (TODO) Need a system for getting/giving feedback, something like gitlab issues?
